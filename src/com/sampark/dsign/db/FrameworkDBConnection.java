package com.sampark.dsign.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.exception.AppException;
import com.sampark.dsign.resources.ResourceReader;

public class FrameworkDBConnection
{
	static Logger logger = Logger.getLogger(FrameworkDBConnection.class);
	private static FrameworkDBConnection CFrameworkConnection;
	private static DataSource Cds = null;
	private static GenericObjectPool CconnectionPool = new GenericObjectPool(null);

	public static FrameworkDBConnection getInstance()
	{
		if (CFrameworkConnection == null) {
			CFrameworkConnection = new FrameworkDBConnection();
		}

		return CFrameworkConnection;
	}

	private static DataSource getDataSource()
			throws Exception
	{
		PoolingDataSource dataSource = null;
		try
		{
			Class.forName(
					ResourceReader.getValueFromBundle(Constants.DB_DRIVER_NAME, 
							Constants.DB_RESOURCE_BUNDLE))
			.newInstance();
			CconnectionPool.setMaxIdle(Integer.parseInt(
					ResourceReader.getValueFromBundle(Constants.DB_POOL_MAX_IDLE, 
							Constants.DB_RESOURCE_BUNDLE).trim()));
			CconnectionPool.setMinIdle(Integer.parseInt(
					ResourceReader.getValueFromBundle(Constants.DB_POOL_MIN_IDLE, 
							Constants.DB_RESOURCE_BUNDLE).trim()));
			CconnectionPool.setMaxWait(Long.valueOf(
					ResourceReader.getValueFromBundle(Constants.DB_POOL_MAX_WAIT, 
							Constants.DB_RESOURCE_BUNDLE).trim()).longValue());

			CconnectionPool.setMaxActive(Integer.parseInt(
					ResourceReader.getValueFromBundle(Constants.DB_POOL_MAX_ACTIVE, 
							Constants.DB_RESOURCE_BUNDLE).trim()));
			CconnectionPool.setNumTestsPerEvictionRun(
					Integer.valueOf(ResourceReader.getValueFromBundle(
							Constants.DB_POOL_MIN_NUM_TSETS_PER_EVICTION_RUN, 
							Constants.DB_RESOURCE_BUNDLE).trim()).intValue());

			Float minEvictableIdleTimeMillis = Float.valueOf(Float.valueOf(
					ResourceReader.getValueFromBundle(
							Constants.DB_POOL_MIN_EVICTABLE_IDLE_TIME, 
							Constants.DB_RESOURCE_BUNDLE).trim()).floatValue() * 
					60000);
			Float timeBetweenEvictionRunsMillis = Float.valueOf(Float.valueOf(
					ResourceReader.getValueFromBundle(
							Constants.DB_POOL_TIME_BETWEEN_EVICTIONRUNS, 
							Constants.DB_RESOURCE_BUNDLE).trim()).floatValue() * 
					60000);
			CconnectionPool
			.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis
					.longValue());
			CconnectionPool
			.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis
					.longValue());
			if ("true".equalsIgnoreCase(ResourceReader.getValueFromBundle(
					Constants.DB_POOL_TEST_WHILE_IDLE, 
					Constants.DB_RESOURCE_BUNDLE))) {
				CconnectionPool.setTestWhileIdle(true);
			} else {
				CconnectionPool.setTestWhileIdle(false);
			}
			if ("true".equalsIgnoreCase(ResourceReader.getValueFromBundle(
					Constants.DB_POOL_TEST_ON_BORROW, 
					Constants.DB_RESOURCE_BUNDLE))) {
				CconnectionPool.setTestOnBorrow(true);
			} else {
				CconnectionPool.setTestOnBorrow(false);
			}

			//IEncryption encryption = new Encryption();
			String decryptedPassword = 
					ResourceReader.getValueFromBundle(Constants.DB_PASSWORD, 
							Constants.DB_RESOURCE_BUNDLE);

			ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
					ResourceReader.getValueFromBundle(Constants.DB_URL, 
							Constants.DB_RESOURCE_BUNDLE), 
					ResourceReader.getValueFromBundle(Constants.DB_USERNAME, 
							Constants.DB_RESOURCE_BUNDLE), 
					decryptedPassword);

			PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(
					connectionFactory, CconnectionPool, null, null, false, false);
			poolableConnectionFactory.setValidationQuery(
					ResourceReader.getValueFromBundle(Constants.DB_POOL_VALIDATION_QUERY, 
							Constants.DB_RESOURCE_BUNDLE));

			dataSource = new PoolingDataSource(CconnectionPool);
		}
		catch (Exception exp) {
			exp.printStackTrace();
			logger.info("Error when attempting to obtain DB Driver ");
			throw new Exception("");
		}
		return dataSource;
	}

	public Connection getDBConnection()
			throws Exception
	{
		Connection connection = null;
		try {
			if (Cds == null) {
				synchronized (this) {
					if (Cds == null) {
						Cds = getDataSource();
					}
				}
			}
			connection = Cds.getConnection();

			if (connection == null) {
				logger.info(" Exception occured while getting connection.");
				throw new Exception("");
			}
			connection.setAutoCommit(false);
		}
		catch (SQLException sqe) {
			sqe.printStackTrace();
			logger.error("Exception occured while getting connection. " + 
					sqe.getMessage());

			throw new AppException(sqe.getMessage());
		}
		return connection;
	}
	
}
