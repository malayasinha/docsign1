package com.sampark.dsign.util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SignatureUtil {
	public static Long expiringIn(Date endDate) {
		Calendar c = Calendar.getInstance();
		Date today = c.getTime();

		long diff =  - today.getTime() + endDate.getTime();

		long diffDays = diff / (24 * 60 * 60 * 1000);
		
		return diffDays;
	}
	public static boolean isNullOrBlank(String str) {
		if(str==null) {
			return true;
		} else if(str.trim().length()==0) {
			return true;
		} else {
			return false;
		}
	}
	public static String isExistAndHasPermission(String fileName) {
		File file = new File(fileName);
		String message = "";
		
		if(!file.exists()) {
			message  = fileName + " folder does not exist";
		} 
		
		if(message.length()==0) {
			if(!file.canRead()) {
				message = fileName + " folder does not have read permission";
			}
		}
		
		if(message.length()==0) {
			if(!file.canWrite()) {
				message = fileName + " folder does not have write permission";
			}
		}
		
		return message;
	}
	
	
	public static List<File> getAssortedList(String source,String endsWith) {
		File file = new File(source);
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(file.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(endsWith);
			}
		})));
		
		return files;
	}
	public static String isEmpty(String location) {
		String message="";
		List<File> zipFiles = SignatureUtil.getAssortedList(location,".pdf");
		if(zipFiles.size()==0) {
			message = location +" folder does not contain any file for processing";
		}
		
		return message;
	}
	
	public static String getDateStr(String format, int days) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		Date date = cal.getTime();
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		String strDate = sdf.format(date);
		
		return strDate;
	}
	
	public static List<Path> getNAssortedFileList(final Path baseDir,String endsWith) {
		List<Path> tenFirstEntries = null;

		final BiPredicate<Path, BasicFileAttributes> predicate = (path, attrs)
		    -> attrs.isRegularFile() && path.getFileName().endsWith(endsWith);

		try (
		    final Stream<Path> stream = Files.find(baseDir, 1, predicate);
		) {
		    tenFirstEntries = stream.limit(10L).collect(Collectors.toList());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		return tenFirstEntries;
	}
}
