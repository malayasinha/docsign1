package com.sampark.dsign.main;

import java.io.File;

import org.apache.log4j.Logger;

import com.sampark.dsign.dao.DBQueries;
import com.sampark.dsign.model.ProfileBean;
import com.sampark.dsign.model.ProfileSignatories;
import com.sampark.dsign.model.SignatoriesBean;
import com.sampark.dsign.service.HSMService;
import com.sampark.dsign.service.MailService;
import com.sampark.dsign.service.SignDocumentService;
import com.sampark.dsign.util.SignatureUtil;

public class MasterThread {

	static Logger logger = Logger.getLogger(MasterThread.class);
	static Logger durationLogger = Logger.getLogger("DURATION");
	Long t1 = 0L;
	
	public void execute() {
		logger.info("Initialization of master thread started...");
		MailService mailService = new MailService();
		DBQueries dao = new DBQueries();
		String profile = "profile1";
		/*List<ProfileBean> profileList = new ArrayList<>();
		profileList = dao.getProfiles();
		*/
		
		t1 = System.currentTimeMillis();
		
		ProfileBean profileBean = new ProfileBean();
		StringBuilder message =new StringBuilder();
		message.append("");
		System.out.println("Fetching data for profile bean...");
		profileBean = dao.getProfileDetail(profile);
		//check source and destination folder
		System.out.println("Checking rw permission in input folder...");
		message.append(SignatureUtil.isExistAndHasPermission(profileBean.getInputFolder()));
		message.append("\t");
		System.out.println("Checking rw permission in output folder...");
		
		if(profileBean.getOutputFolder().contains("dd-MM-yyyy")) {
			String dateFolder = SignatureUtil.getDateStr("dd-MM-yyyy", -1);
			profileBean.setOutputFolder(profileBean.getOutputFolder().replace("dd-MM-yyyy", dateFolder));
			if(!new File(profileBean.getOutputFolder()).exists()) {
				if(new File(profileBean.getOutputFolder()).mkdirs()) {
					logger.info(profileBean.getOutputFolder()+ " created successfully.");
				} else {
					logger.info(profileBean.getOutputFolder()+ " could not be created.");
					return;
				}
			} else {
				logger.info(profileBean.getOutputFolder() +" already exist");
			}
		} else {
			logger.info("Dynamic folder dd-MM-yyyy not found. Left the folder structure unchanged.");
			
		}
		
		message.append(SignatureUtil.isExistAndHasPermission(profileBean.getOutputFolder()));
		message.append("\t");
		
		logger.info(message.toString());
		//if source and destination folders are available then find if the folder is empty
		System.out.println("Checking if empty or blank folder...");
		if(SignatureUtil.isNullOrBlank(message.toString())) {
			message.append(SignatureUtil.isEmpty(profileBean.getInputFolder()));
			message.append("\t");
		} else {
			logger.info(message.toString());
		}
		ProfileSignatories profileSign = new ProfileSignatories();
		profileSign = dao.getProfileSign(profileBean.getRowId());
		
		SignatoriesBean signatories = new SignatoriesBean();
		signatories = dao.getSignatories(profileSign.getSignId());
		
		logger.info("Details for signing document: "+profileBean);
		logger.info("Details for signature from HSM: "+profileSign);
		long days = 0L;
		//check if the signature has expired
		if(signatories.getEndDate() != null) {
			days = SignatureUtil.expiringIn(signatories.getEndDate());
			logger.info("signature expiring in "+days +" days");
			if(days <= 0) {
				message.append(signatories.getName() +" Signature has expired.");
				message.append("\t");
			} else {
				logger.info(message.toString());
				//getting signature from HSM Device
				signatories = HSMService.readPrivateKeyFromHSM(signatories);
			}
		}
		
		//check if HSM device is available and certificate are intact
		if(SignatureUtil.isNullOrBlank(signatories.getMessage())){
			message.append(signatories.getMessage());
			message.append("\t");
		}
		logger.info(message.toString());
		profileSign.setSignBean(signatories);
		
		logger.info(message.toString());
		if(SignatureUtil.isNullOrBlank(message.toString())) {
			new SignDocumentService().processDocument(profileBean, profileSign);
			logger.info(message);
		} else {
			mailService.prepareMail("error", profileBean.getRowId(), message.toString());
		}
		
		if(days > 0 && days < 90) {
			mailService.prepareMail("error", profileBean.getRowId(), profileSign.getSignBean().getName()+" digital signature is expiring in "+days+" days");
		}
		
		profileBean = null;
		profileSign = null;
		
		logger.info("Details for signing document: "+profileBean);
		
		logger.info("Details for signature from HSM: "+profileSign);
		
		logger.info("Pdf file copy stopped");
		
	}
}
