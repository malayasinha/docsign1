package com.sampark.dsign.service;

import java.io.File;
import java.io.FilenameFilter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.sampark.dsign.constants.Constants;
import com.sampark.dsign.model.ProfileBean;
import com.sampark.dsign.model.ProfileSignatories;
import com.sampark.dsign.thread.SignDocumentThread;
import com.sampark.dsign.util.SignatureUtil;
import com.sampark.dsign.util.ZipUtility;

public class SignDocumentService {
	static Logger logger = Logger.getLogger(DocumentService.class);
	static Logger durationLogger = Logger.getLogger("DURATION");
	Long t1 = 0L;
	Long batchSize = 25000L;
	
	public void processDocument(ProfileBean profile, ProfileSignatories profileSign) {
		logger.info("StartTime:"+new Timestamp(System.currentTimeMillis()));
		MailService mailService = new MailService();
		
		ExecutorService executor = Executors.newFixedThreadPool(Constants.THREADS);
		String sourcePath = profile.getInputFolder();
		//String destinationPath = profile.getOutputFolder()+File.separator+SignatureUtil.getDateStr("dd-MM-yyyy", -1);
		String destinationPath = profile.getOutputFolder();
		
		logger.info("Destination path "+destinationPath);
		
		//This will process the files in batches
		//List<File> fList = new ArrayList<>();
		List<File> fileList = SignatureUtil.getAssortedList(sourcePath,".pdf");
		/*try {
			fList = SignatureUtil.getAssortedList(sourcePath,".pdf");
			for(int i=0;i<fList.size();i++) {
				fileList.add(fList.get(i));
				if(i==batchSize) {
					break;
				}
			}
		} catch (NullPointerException npx) {
			logger.info("The source folder cannot be accessed at the moment.");
			//mailService.prepareMail("error", profile.getRowId(), "The source folder cannot be accessed at the moment.");
			//configure alert
		}*/
		durationLogger.info("Time to fetch List of files from file system"+(System.currentTimeMillis() - t1));
				
		logger.info("processing list of items:"+fileList.size());
		//System.exit(0);
		
		
		Iterator<File> fileItr = fileList.iterator();
		if(fileList!=null && fileList.size() > 0) {
			while(fileItr.hasNext()) {
				File file = fileItr.next();
				try {
					
					Runnable worker = new SignDocumentThread(sourcePath+File.separator+file.getName(), destinationPath+File.separator+file.getName(),profileSign);
		            executor.execute(worker);
				} catch (Exception e) {
					logger.info(e.getMessage());
					e.printStackTrace();
				}
			}
			executor.shutdown();
	        while (!executor.isTerminated()) {
	        
	        }
	        //dao.getFailedCount(profile.getRowId(), new Date());
	        Integer [] arg = {fileList.size() - Constants.Failed,Constants.Failed,fileList.size()};
	        mailService.prepareMail("info", profile.getRowId(), arg);
	        
	        logger.info("Creating zip file for the processed files");
	        ZipUtility zipFiles = new ZipUtility();
			zipFiles.createTarFile(fileList,profile.getInputFolder());
			//zipFiles.deleteOldGZipFiles();
			
		} else {
			logger.info("Could not connect to the SAN Storage");
			//configure alert
		}
        
	}
	
	private List<File> getList(File source) {
		//ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles()));
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(source.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".pdf");
			}
		})));	
		
		Collections.sort(files, new Comparator<File>() {
		    @Override
		    public int compare(File o1, File o2) {
		        Long l1 = (Long)o1.lastModified();
		        Long l2 = (Long)o2.lastModified();
		    	return l1.compareTo(l2);
		    }
		});
		if(files.size()>=10000) {
			return files.subList(0, 50);
		} else {
			return files.subList(0, files.size());
		}
	}

/*	public void signPdf(String src, String dest, ProfileSignatories profileSign) throws Exception {
		PdfReader reader = new PdfReader(src);
		PdfStamper stamper = PdfStamper.createSignature(reader, new FileOutputStream(dest), '\0');
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		cal.setTime(date);

		int m = stamper.getReader().getNumberOfPages();

		//appearance.setVisibleSignature(new Rectangle(300, 200, 400, 300), m, "Signature");
		
		appearance.setVisibleSignature(new Rectangle(profileSign.getxAxis(), profileSign.getyAxis(), profileSign.getxAxis()+profileSign.getSignSize(), profileSign.getyAxis()+profileSign.getSignSize()), 
				m, "Signature");
		appearance.setSignDate(cal);
		appearance.setReason("It's test.");
		appearance.setLocation("Gurgaon");
		appearance.setAcro6Layers(false);
		appearance.setLayer4Text(PdfSignatureAppearance.questionMark);
		appearance.setCertificationLevel(PdfSignatureAppearance.CERTIFIED_NO_CHANGES_ALLOWED);
		
		ExternalSignature es = new PrivateKeySignature(profileSign.getSignBean().getPrivateKey(), "SHA-256", "LunaProvider");
		ExternalDigest digest = new BouncyCastleDigest();
		MakeSignature.signDetached(appearance, digest, es, profileSign.getSignBean().getCert(), null, null, null, 0, CryptoStandard.CMS);

	}
	public void processDocument(ProfileBean profile, List<ProfileSignatories> profilesign) {
		System.out.println("StartTime:"+new Timestamp(System.currentTimeMillis()));
		ExecutorService executor = Executors.newFixedThreadPool(50);
		
		File sourcePath = new File(profile.getInputFolder());
		String destinationPath = profile.getOutputFolder();
		
			for (final File file : sourcePath.listFiles()) {
				try {
					Runnable worker = new SignDocumentThread(sourcePath+File.separator+file.getName(), destinationPath+File.separator+file.getName(),profilesign);
		            executor.execute(worker);
				} catch (Exception e) {
					logger.info(e.getMessage());
					e.printStackTrace();
				}
				
			}
		
		executor.shutdown();
        while (!executor.isTerminated()) {
        
        }
        System.out.println("EndTime:"+new Timestamp(System.currentTimeMillis()));

	}

	*/
	
}
