package com.sampark.dsign.model;

import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class SignatoriesBean {
	private Integer signId;
	private String name;
	private String privateKeyLabel;
	private String certificateLabel;
	private String email;
	private PrivateKey privateKey;
	private Certificate[] cert;
	private Boolean hasSignature;
	private Date startDate;
	private Date endDate;
	private String message;
	
	public Integer getSignId() {
		return signId;
	}
	public void setSignId(Integer signId) {
		this.signId = signId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPrivateKeyLabel() {
		return privateKeyLabel;
	}
	public void setPrivateKeyLabel(String privateKeyLabel) {
		this.privateKeyLabel = privateKeyLabel;
	}
	public String getCertificateLabel() {
		return certificateLabel;
	}
	public void setCertificateLabel(String certificateLabel) {
		this.certificateLabel = certificateLabel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public PrivateKey getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}
	public Certificate[] getCert() {
		return cert;
	}
	public void setCert(Certificate[] cert) {
		this.cert = cert;
	}
	public Boolean getHasSignature() {
		return hasSignature;
	}
	public void setHasSignature(Boolean hasSignature) {
		this.hasSignature = hasSignature;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public SignatoriesBean(Integer signId, String name, String privateKeyLabel, String email,String certificateLabel) {
		super();
		this.signId = signId;
		this.name = name;
		this.privateKeyLabel = privateKeyLabel;
		this.certificateLabel = certificateLabel;
		this.email = email;
	}
	public SignatoriesBean() {
	}
	@Override
	public String toString() {
		return "SignatoriesBean [signId=" + signId + ", name=" + name + ", privateKeyLabel=" + privateKeyLabel
				+ ", certificateLabel=" + certificateLabel + ", email=" + email + ", privateKey=" + privateKey
				+ ", cert=" + Arrays.toString(cert) + ", hasSignature=" + hasSignature + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
	
	
}
