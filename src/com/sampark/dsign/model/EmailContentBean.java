package com.sampark.dsign.model;

import java.util.List;

public class EmailContentBean {
	private List<ProfileMailBean> emailList;
	private String subject;
	private String body;
	
	public List<ProfileMailBean> getEmailList() {
		return emailList;
	}
	public void setEmailList(List<ProfileMailBean> emailList) {
		this.emailList = emailList;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	@Override
	public String toString() {
		return "EmailContentBean [emailList=" + emailList + ", subject=" + subject + ", body=" + body + "]";
	}
	
	
	
}
