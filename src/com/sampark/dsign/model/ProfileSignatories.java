package com.sampark.dsign.model;

public class ProfileSignatories {
	private Integer rowId;
	private Integer profileId;
	private Integer signId;
	private SignatoriesBean signBean;
	private Integer xAxis;
	private Integer yAxis;
	private Integer signSize;
	private String pages;
	public Integer getRowId() {
		return rowId;
	}
	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}
	public Integer getProfileId() {
		return profileId;
	}
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}
	public Integer getSignId() {
		return signId;
	}
	public void setSignId(Integer signId) {
		this.signId = signId;
	}
	public SignatoriesBean getSignBean() {
		return signBean;
	}
	public void setSignBean(SignatoriesBean signBean) {
		this.signBean = signBean;
	}
	public Integer getxAxis() {
		return xAxis;
	}
	public void setxAxis(Integer xAxis) {
		this.xAxis = xAxis;
	}
	public Integer getyAxis() {
		return yAxis;
	}
	public void setyAxis(Integer yAxis) {
		this.yAxis = yAxis;
	}
	public Integer getSignSize() {
		return signSize;
	}
	public void setSignSize(Integer signSize) {
		this.signSize = signSize;
	}
	public String getPages() {
		return pages;
	}
	public void setPages(String pages) {
		this.pages = pages;
	}
	@Override
	public String toString() {
		return "ProfileSignatories [rowId=" + rowId + ", profileId=" + profileId + ", signId=" + signId + ", signBean="
				+ signBean + ", xAxis=" + xAxis + ", yAxis=" + yAxis + ", signSize=" + signSize + ", pages=" + pages
				+ "]";
	}
	
	
	
	
}
