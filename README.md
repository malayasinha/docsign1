# Document sign using itext and integration with HSM

## Features

- A scheduled java service fetches pdf files from the file system
- The files are signed using itext and stores it in another file system
- The application sends mail to the stakeholders about the signed status

## Softwares requried to run this application

- JDK
- Eclipse
- Git
- Gemalto HSM 

## Installation

- Clone the git repository
- Import the application in eclipse
- Run the application
